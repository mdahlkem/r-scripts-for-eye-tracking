# R Scripts for Eye Tracking with Feynman diagrams

## Description
The scripts in this project have been used to analyse the eye tracking data obtained from the third student study within my PhD thesis "The educational Use of Feynman Diagrams: Opportunities, Challenges, Practices".

## Disclaimer
The code published in this repository was written in a physicist's mentality: It runs, so it's fine. It was originally not written to be published. I have made some efforts to tidy it up, but I have to give out the warning that it is still difficult to read.

## Installation
To use these Scripts, the R Software package is needed. Additionally, some special R packages are needed which are specified in the beginning of each file.

## Usage
The Script analyses Eye Tracking and questionnaire data. These data are available under [this link](https://doi.org/10.5281/zenodo.11503036) for the third student study and under [this link](https://doi.org/10.5281/zenodo.11505981) for the first and second student study.
There are three types of data files: Raw Eye tracking data, Metrics, and Questionnaire data:
* To process the raw ET data, first the ```et_data_cleaning_preprocessing.R``` script has to be run in order to create files which can be further processed (I did that because the working memory of the computer I've been working with couldn't handle the big files all at once). Subsequently, the files can be preprocessed with the ```et_data_cleaning.R``` script. The analysis of these files is then carried out in the ```et_entropy_calculation.R``` script. The purpose of this script is to calculate transition-based metrics such as entropy, triple transition ratios, and fixation/transition ratios. For further details see the respective part of the thesis.
* The metrics files are processed using the ```et_metrics_analysis.R``` file. The purpose of this is to find durations on certain areas of interest, dependent on certain participant variables such as whether they answered correctly.
* The questionnaire files are processed in the ```quantitative-questionnaire-data.R``` script. In there, the data from the prior knowledge questionnaire, the cognitive load questionnaire and the motivation instrument are analysed.

## Support
In case of any questions, reach out to Merten Dahlkemper, under merten.nikolay@gmail.com.



